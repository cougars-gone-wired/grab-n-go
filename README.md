# Grab 'n' Go

Our trusty and faithful robot Grab 'n' Go has seen many versions of code, including Control Systems upgrades, development platform upgrades, and even WPILib upgrades. This repository contains the latest Grab 'n' Go code and will be the only Grab 'n' Go repository going forth.

# Port Loadout 

| Motor             | Port |
|-------------------|------|
| Left Front Motor  | 01   |
| Right Front Motor | 02   |
| Left Rear Motor   | 03   |
| Right Rear Motor  | 04   |
| Shoulder Motor 1  | 10   |
| Shoulder Motor 2  | 11   |
| Elbow Motor       | 12   |
| Claw Motor        | 13   |

# Setup Guide

1. Place the test board. The roboRIO should be on the right side of the robot. Remember: "Rio Goes Right!" If you place it in the wrong way, you cannot plug in certain motors.
2. Plug in the motor controllers in the back of the robot. Remember that they are plugged in from left to right, and that red goes to red, and black goes to black. Any overlapping will result in something not working. 
3. Plug in the drive motors. They should be labelled 01, 02, 03, and 04 to help you!
4. Splice in the CAN bus. The wires are much smaller, so they may be hard to find. The CAN wires on the robot are near the motor controllers that you plugged into the test board in step 2. Remember to make a full loop (the CAN loop), or else you will have issues. 
5. Place a battery face down in the front of the robot and plug it in! Turn on the robot, deploy any new code, and Grab 'n' Go should be alive again!