package frc.robot;

import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import edu.wpi.first.wpilibj.drive.MecanumDrive;

public class Drive {

    private WPI_TalonSRX frontLeftMotor;
    private WPI_TalonSRX backLeftMotor;
    private WPI_TalonSRX frontRightMotor;
    private WPI_TalonSRX backRightMotor;

    private MecanumDrive robotDrive;

    public Drive() {
            frontLeftMotor = new WPI_TalonSRX(Constants.DriveConstants.LEFT_FRONT_MOTOR_ID);
            frontLeftMotor.setNeutralMode(NeutralMode.Brake);
            frontLeftMotor.configOpenloopRamp(Constants.DriveConstants.RAMP_TIME);
    
            backLeftMotor = new WPI_TalonSRX(Constants.DriveConstants.LEFT_REAR_MOTOR_ID);
            //backLeftMotor.setInverted(true);
            backLeftMotor.setNeutralMode(NeutralMode.Brake);
            backLeftMotor.configOpenloopRamp(Constants.DriveConstants.RAMP_TIME);
    
            frontRightMotor = new WPI_TalonSRX(Constants.DriveConstants.RIGHT_FRONT_MOTOR_ID);
            frontRightMotor.setInverted(true);
            frontRightMotor.setNeutralMode(NeutralMode.Brake);
            frontRightMotor.configOpenloopRamp(Constants.DriveConstants.RAMP_TIME);
    
            backRightMotor =  new WPI_TalonSRX(Constants.DriveConstants.RIGHT_REAR_MOTOR_ID);
            backRightMotor.setInverted(true);
            backRightMotor.setNeutralMode(NeutralMode.Brake);
            backRightMotor.configOpenloopRamp(Constants.DriveConstants.RAMP_TIME);

            robotDrive = new MecanumDrive(frontLeftMotor, backLeftMotor, frontRightMotor, backRightMotor);
            robotDrive.setDeadband(Constants.DriveConstants.DEADZONE);
            robotDrive.setSafetyEnabled(false);
    }

    public void initalize() {
        frontLeftMotor.set(0);
        frontRightMotor.set(0);
        backLeftMotor.set(0);
        backRightMotor.set(0);
    }

    public void robotDrive() {
        double driveSpeedAxis = -(Robot.mobilityController.getRawAxis(Constants.ControllerConstants.MAIN_AXIS_ID)) * Constants.DriveConstants.MAIN_AXIS_SPEED;
        double driveStrafeAxis = (Robot.mobilityController.getRawAxis(Constants.ControllerConstants.STRAFE_AXIS_ID)) * Constants.DriveConstants.STRAFE_AXIS_SPEED;
        double driveYawAxis = (Robot.mobilityController.getRawAxis(Constants.ControllerConstants.YAW_AXIS_ID)) * Constants.DriveConstants.YAW_AXIS_SPEED;
        robotDrive.driveCartesian(driveSpeedAxis, driveStrafeAxis, driveYawAxis);
    }
}
