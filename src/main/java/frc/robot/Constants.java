// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {

    public class ControllerConstants {

        // Mobility Controller
        static final int MOBILITY_CONTROLLER_ID = 0;
        static final int YAW_AXIS_ID = 4;
        static final int MAIN_AXIS_ID = 1;
        static final int STRAFE_AXIS_ID = 0;
    
        // Manipulator Controller
        static final int MANIPULATOR_CONTROLLER_ID = 1;
        static final int SHOULDER_UP_BUTTON_ID = 1;
        static final int SHOULDER_DOWN_BUTTON_ID = 2;
        static final int ELBOW_UP_BUTTON_ID = 5;
        static final int ELBOW_DOWN_BUTTON_ID = 6;
        static final int CLAW_UP_BUTTON_ID = 3;
        static final int CLAW_DOWN_BUTTON_ID = 4;
    }

    public class DriveConstants {

         // Drive motor controller IDs go here. NOTE: Maybe should be on Settings page on Dashboard?
         public static final int LEFT_FRONT_MOTOR_ID = 1;
         public static final int RIGHT_FRONT_MOTOR_ID = 2;
 
         public static final int LEFT_REAR_MOTOR_ID = 3;
         public static final int RIGHT_REAR_MOTOR_ID = 4;
 
         public static final double MAIN_AXIS_SPEED = 0.50;
         public static final double STRAFE_AXIS_SPEED = 0.50;
         public static final double YAW_AXIS_SPEED = 0.50;
         public static final double RAMP_TIME = 0.00;
         public static final double DEADZONE = 0.30;
         
         // public static final double DEADZONE = 0.15; // Last year's value was 0.15

    }

    public class ArmConstants {
        public static final int SHOULDER_MOTOR1_ID = 10;
        public static final int SHOULDER_MOTOR2_ID = 11;
        public static final int ELBOW_MOTOR_ID = 12;
        public static final int CLAW_MOTOR_ID = 13;

        public static final double CLAW_MOTOR_SPEED = 0.5;
        public static final double ELBOW_MOTOR_SPEED = 0.4;
        public static final double SHOULDER_MOTOR_SPEED = 0.3;
    }
}