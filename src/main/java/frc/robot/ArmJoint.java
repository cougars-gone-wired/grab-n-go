package frc.robot;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import edu.wpi.first.wpilibj.motorcontrol.MotorControllerGroup;

import com.ctre.phoenix.motorcontrol.NeutralMode;

public class ArmJoint {
    private int controllerUpButton;
    private int controllerDownButton;
    private double motorSpeed;

    private WPI_TalonSRX shoulderMotor1;
    private WPI_TalonSRX shoulderMotor2;
    private MotorControllerGroup shoulderMotors;

    public enum ArmJointStates { MOVING_UP, NOT_MOVING, MOVING_DOWN }
    private ArmJointStates currentArmJointState;
    
    public ArmJoint(int shoulderMotor1, int controllerUpButton, int controllerDownButton, double motorSpeed) {
        _ArmJoint(shoulderMotor1, -1);
        this.controllerUpButton = controllerUpButton;
        this.controllerDownButton = controllerDownButton;
        this.motorSpeed = motorSpeed;
    }

    public ArmJoint(int shoulderMotor1, int shoulderMotor2, int controllerUpButton, int controllerDownButton, double motorSpeed) {
        _ArmJoint(shoulderMotor1, shoulderMotor2);
        this.controllerUpButton = controllerUpButton;
        this.controllerDownButton = controllerDownButton;
        this.motorSpeed = motorSpeed;
    }

    private void _ArmJoint(int shoulderMotorID1, int shoulderMotorID2) {
        shoulderMotor1 = new WPI_TalonSRX(shoulderMotorID1);
        shoulderMotor1.setInverted(true);
        
        if(shoulderMotorID2 > 0) {
            shoulderMotor2 = new WPI_TalonSRX(shoulderMotorID2);
            shoulderMotor2.setInverted(false);
            shoulderMotors = new MotorControllerGroup(shoulderMotor1 , shoulderMotor2);
        } else {
            shoulderMotors = new MotorControllerGroup(shoulderMotor1);
        }

        initialize();
    }

    public void initialize() {
        setNotMoving();
        setMotorsBrake();
        currentArmJointState = ArmJointStates.NOT_MOVING;
    }

    public void controlTeleop() {
        boolean upPressed = Robot.manipulatorController.getRawButton(controllerUpButton);
        boolean downPressed = Robot.manipulatorController.getRawButton(controllerDownButton);
        
        switch (currentArmJointState) {
            case MOVING_UP:

                if (!upPressed) setNotMoving();
                else if (downPressed) setNotMoving();

                shoulderMotors.set(motorSpeed);
                break;
            
            case NOT_MOVING:
                
                if(upPressed && !downPressed) setMovingUp();
                else if(downPressed && !upPressed) setMovingDown();

                shoulderMotors.set(0);
                break;
            
            case MOVING_DOWN:
                
                if (!downPressed) setNotMoving();
                else if (upPressed) setNotMoving();

                shoulderMotors.set(-motorSpeed);
                break;
        }
    }

    // Control Settings
    public void setMotorsBrake() {
        shoulderMotor1.setNeutralMode(NeutralMode.Brake);
        if(shoulderMotor2 != null) {
            shoulderMotor2.setNeutralMode(NeutralMode.Brake);
        }
    }

    public void setMotorsCoast() {
        shoulderMotor1.setNeutralMode(NeutralMode.Coast);
        if(shoulderMotor2 != null) { 
            shoulderMotor2.setNeutralMode(NeutralMode.Coast);
        }
    }

    // Setters
    public void setMovingUp() {
        currentArmJointState = ArmJointStates.MOVING_UP;
        
    }

    public void setNotMoving() {
        currentArmJointState = ArmJointStates.NOT_MOVING;
    }

    public void setMovingDown() {
        currentArmJointState = ArmJointStates.MOVING_DOWN;
    }
}