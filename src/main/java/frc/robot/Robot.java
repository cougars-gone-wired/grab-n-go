// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * The VM is configured to automatically run this class, and to call the functions corresponding to
 * each mode, as described in the TimedRobot documentation. If you change the name of this class or
 * the package after creating this project, you must also update the build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {
  private static final String kDefaultAuto = "Default";
  private static final String kCustomAuto = "My Auto";
  private String m_autoSelected;
  private final SendableChooser<String> m_chooser = new SendableChooser<>();

  public static Joystick mobilityController;
  public static Joystick manipulatorController;

  public static Drive drive;
  public static ArmJoint armJoint;
  public static ArmJoint elbowJoint;
  public static ArmJoint claw;

  /**
   * This function is run when the robot is first started up and should be used for any
   * initialization code.
   */
  @Override
  public void robotInit() {
    m_chooser.setDefaultOption("Default Auto", kDefaultAuto);
    m_chooser.addOption("My Auto", kCustomAuto);
    SmartDashboard.putData("Auto choices", m_chooser);

    mobilityController = new Joystick(Constants.ControllerConstants.MOBILITY_CONTROLLER_ID);
    manipulatorController = new Joystick(Constants.ControllerConstants.MANIPULATOR_CONTROLLER_ID);

    drive = new Drive();
    armJoint = new ArmJoint(Constants.ArmConstants.SHOULDER_MOTOR1_ID, Constants.ArmConstants.SHOULDER_MOTOR2_ID, 
                            Constants.ControllerConstants.SHOULDER_UP_BUTTON_ID, Constants.ControllerConstants.SHOULDER_DOWN_BUTTON_ID, 
                            Constants.ArmConstants.SHOULDER_MOTOR_SPEED);
    elbowJoint = new ArmJoint(Constants.ArmConstants.ELBOW_MOTOR_ID, Constants.ControllerConstants.ELBOW_UP_BUTTON_ID, 
                              Constants.ControllerConstants.ELBOW_DOWN_BUTTON_ID, Constants.ArmConstants.ELBOW_MOTOR_SPEED);
    claw = new ArmJoint(Constants.ArmConstants.CLAW_MOTOR_ID, Constants.ControllerConstants.CLAW_UP_BUTTON_ID, 
                        Constants.ControllerConstants.CLAW_DOWN_BUTTON_ID, Constants.ArmConstants.CLAW_MOTOR_SPEED);
  }

  /**
   * This function is called every robot packet, no matter the mode. Use this for items like
   * diagnostics that you want ran during disabled, autonomous, teleoperated and test.
   *
   * <p>This runs after the mode specific periodic functions, but before LiveWindow and
   * SmartDashboard integrated updating.
   */
  @Override
  public void robotPeriodic() {}

  /**
   * This autonomous (along with the chooser code above) shows how to select between different
   * autonomous modes using the dashboard. The sendable chooser code works with the Java
   * SmartDashboard. If you prefer the LabVIEW Dashboard, remove all of the chooser code and
   * uncomment the getString line to get the auto name from the text box below the Gyro
   *
   * <p>You can add additional auto modes by adding additional comparisons to the switch structure
   * below with additional strings. If using the SendableChooser make sure to add them to the
   * chooser code above as well.
   */
  @Override
  public void autonomousInit() {
    m_autoSelected = m_chooser.getSelected();
    // m_autoSelected = SmartDashboard.getString("Auto Selector", kDefaultAuto);
    System.out.println("Auto selected: " + m_autoSelected);
  }

  /** This function is called periodically during autonomous. */
  @Override
  public void autonomousPeriodic() {
    switch (m_autoSelected) {
      case kCustomAuto:
        // Put custom auto code here
        break;
      case kDefaultAuto:
      default:
        // Put default auto code here
        break;
    }
  }

  /** This function is called once when teleop is enabled. */
  @Override
  public void teleopInit() {

    drive.initalize();
    armJoint.initialize();
    elbowJoint.initialize();
    claw.initialize();
  }

  /** This function is called periodically during operator control. */
  @Override
  public void teleopPeriodic() {

    drive.robotDrive();
    armJoint.controlTeleop();
    elbowJoint.controlTeleop();
    claw.controlTeleop();
  }

  /** This function is called once when the robot is disabled. */
  @Override
  public void disabledInit() {}

  /** This function is called periodically when disabled. */
  @Override
  public void disabledPeriodic() {}

  /** This function is called once when test mode is enabled. */
  @Override
  public void testInit() {}

  /** This function is called periodically during test mode. */
  @Override
  public void testPeriodic() {}
}
